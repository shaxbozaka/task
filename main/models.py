from django.db import models
from django.core.validators import RegexValidator


class Tag(models.Model):
    tag = models.CharField(max_length=100)

    def __str__(self):
        return self.tag


class Customer(models.Model):
    phone_regex = RegexValidator(regex=r'^\+\d{1,3}', message="Phone number code must be entered in the format: nothing")
    mail = models.EmailField()
    phone_code = models.CharField(validators=[phone_regex], blank=True, max_length=4)
    phone_number = models.CharField(max_length=9, blank=True)
    tag = models.ManyToManyField(Tag)
    utc = models.IntegerField()

    def __str__(self):
        return self.mail


class MailModel(models.Model):
    sendmail = models.DateTimeField()
    delivered = models.DateTimeField()
    text = models.TextField()
    tag = models.ManyToManyField(Tag)
    status = models.BooleanField(default=False)
    customer = models.ManyToManyField(Customer)

    def __str__(self):
        return self.text


class Sms(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField()
    mail = models.ManyToManyField(MailModel)
    customer = models.ForeignKey(Customer, models.CASCADE)
