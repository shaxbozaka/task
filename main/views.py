from rest_framework.viewsets import ModelViewSet
from .models import MailModel, Tag, Customer
from .serializer import MainSerializer


class MailView(ModelViewSet):
    queryset = MailModel.objects.all()
    serializer_class = MainSerializer

    def __init__(self, *args, **kwargs):
        MainSerializer.Meta.model = MailModel


class CustomerView(ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = MainSerializer

    def __init__(self, *args, **kwargs):
        MainSerializer.Meta.model = Customer
