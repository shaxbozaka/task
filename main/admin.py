from django.contrib import admin
from .models import MailModel, Customer, Tag

admin.site.register(MailModel)
admin.site.register(Customer)
admin.site.register(Tag)
