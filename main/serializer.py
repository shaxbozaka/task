from rest_framework.serializers import ModelSerializer
from .models import Customer, Tag, MailModel


class MainSerializer(ModelSerializer):
    class Meta:
        fields = '__all__'

