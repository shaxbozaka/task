from .views import MailView, CustomerView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('mail', MailView, basename='mail')
router.register('customer', CustomerView, basename='customer')
urlpatterns = [
] + router.urls

