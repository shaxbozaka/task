#!/usr/bin/env python3
import os
import subprocess

port = 8000
os.system('python ./manage.py makemigrations')
os.system('python ./manage.py migrate')
print(f"session started at {port} port!!!")
one = subprocess.run(f"python ./manage.py runserver 0.0.0.0:{port} | python ./manage.py async_task", shell=True)

print("session finished!!!")