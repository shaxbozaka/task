# shaxbozaka/task

# system requirements
### python, psql


## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requirements.txt, before we have to create environment let's do it.

```
python3 -m venv .env
```

And exactly we will activate it.
### linux
```
source .env/bin/activate
```
### windows
```
.\env\Scripts\activate
```

So, we can install requirements
``` bash
pip install -r requirements.txt
```

## Usage
```
set POSTGRES_NAME=postgres
set POSTGRES_USER=postgres_user
set POSTGRES_PASSWORD=postgres_pwd
set HOST=127.0.0.1

python run.py

```
## All done server listen on ```localhost:8000```, but if you use docker, set HOST to 'db' > ```set HOST=db```

## URLS

```html

mail/ allowed methods [GET, POST]
customer/ allowed methods [GET, POST]

mail/<pk> allowed method GET, POST]
customer/<pk> allowed method GET, POST]

It is not all, Also I have turned on DEBUG. 
```

# Techlogies
### DJANGO, DRF, DJANGO_SMTP_SERVICE, JSON, PSQL

# DOCKERIZE

```
sudo docker-compose up
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
